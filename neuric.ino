#include <SoftPWM.h>

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  
 * ATtiny44 pinout
 *                           +-\/-+
 *                     VCC  1|    |14  GND
 *             (D 10)  PB0  2|    |13  PA0  (D  0)        AREF
 *             (D  9)  PB1  3|    |12  PA1  (D  1) 
 *                     PB3  4|    |11  PA2  (D  2) 
 *  PWM  INT0  (D  8)  PB2  5|    |10  PA3  (D  3) 
 *  PWM        (D  7)  PA7  6|    |9   PA4  (D  4) 
 *  PWM        (D  6)  PA6  7|    |8   PA5  (D  5)        PWM
 *                           +----+
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * NeuroByte v04 pinout
 *  __________     _____________     __________     __________
 * | K5 K3 K1 |   | PA1 PA3 PA5 |   | 12 10  8 |   | D1 D3 D5 |
 * | K6 K4 K2 |   | PA0 PA2 PA4 |   | 13 11  9 |   | D0 D2 D4 |
 * |          |   |             |   |          |   |          |
 * |_K7_______|   |_PA6_________|   |__7_______|   |_D6_______|
 * 
 *   Headers           Ports          Physical       Arduino
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* mV values for important points in the membrane potential range
   used for declaration of arrays below. */
#define V_MIN -100
#define V_HALF_MR -85 // halfway point between min and rest
#define V_REFRACTORY -85
#define V_REST -70
#define V_HALF_RT -55 // halfway point between rest and thresh
#define V_THRESH -40
#define V_PEAK 55
#define INTERVAL 15
#define INTERVAL_NO 4

/* multipliers - see below. keep 'em in base 2 */
#define MULT_PWM 4
#define MULT_RES 32
//#define MULT_RES 1
/* used to convert between mem. potential values in mV
   and mem. potential values used in code */
#define COEFF_MV (MULT_PWM/2)*MULT_RES*INTERVAL_NO

#define ADD_V = abs(V_MIN)
/* number of PWM levels. */
#define PWM_LEVELS abs(V_MIN-V_THRESH)*MULT_PWM

/* indexes for important points in the membrane potential range
   for usage of arrays declared below. */
#define INDEX_MIN 0
#define INDEX_HALF_MR 1
#define INDEX_REFRACTORY 1
#define INDEX_REST 2
#define INDEX_HALF_RT 3
#define INDEX_THRESH 4
#define INDEX_PEAK 5

/* which LED color corresponds to which channel */
#define RED 0
#define GREEN 1
#define BLUE 2

/* these are the three LED output channels: */
SOFTPWM_DEFINE_CHANNEL_INVERT(0, DDRB, PORTB, PB1); // PB1, red
SOFTPWM_DEFINE_CHANNEL_INVERT(1, DDRB, PORTB, PB0); // PB0, green
SOFTPWM_DEFINE_CHANNEL_INVERT(2, DDRB, PORTB, PB2); // PB2, blue
SOFTPWM_DEFINE_OBJECT_WITH_PWM_LEVELS(3, PWM_LEVELS+1); // channels, levels

/* dendritic input pins set to 1 in this mask. to only read
   dendrite input pins from PINA, do PINA & DENDRITE_PINS_MASK */
#define DENDRITE_PINS_MASK 0b00111111

/* membrane potential values in mV
 * bit of an explanation:
 * | ...
 * | AP!
 * | AP!
 * |---------------- V_THRESH =  -40 mV
 * | INTERVAL
 * |------------ V_HALF_RT
 * | INTERVAL
 * |---------------- V_REST   =  -70 mV
 * | INTERVAL
 * |------------ V_HALF_MR
 * | INTERVAL
 *  ---------------- V_MIN    = -100 mV
 * 
 * set thresh, rest and min so that INTERVALs are equal!
 * 
 * we level the scale off so it starts at 0 and multiply by
 * MULT_PWM and INTERVAL_NO so that each INTERVAL is
 * PWM_LEVELS/2. this gives a v_current resolution of
 * 4*PWM_LEVELS/2, and facilitates calculation of LED PWM levels.
 * 
 * here's an array that holds these values:
 */
const int16_t v_values[5] =
        { 0,   // INDEX_MIN, [0]
         (V_HALF_MR+abs(V_MIN)) * MULT_PWM/2 * MULT_RES * INTERVAL_NO,   // INDEX_HALF_MR, [1]
         (V_REST+abs(V_MIN))    * MULT_PWM/2 * MULT_RES * INTERVAL_NO,   // INDEX_REST, [2]
         (V_HALF_RT+abs(V_MIN)) * MULT_PWM/2 * MULT_RES * INTERVAL_NO,   // INDEX_HALF_RT, [3]
         (V_THRESH+abs(V_MIN))  * MULT_PWM/2 * MULT_RES * INTERVAL_NO };   // INDEX_THRESH, [4]
int16_t v_current = v_values[INDEX_REST]; // current v = resting v
int16_t v_target; // new resting potentatial based on dendritic input

/* whether or not to fire an AP */
uint8_t flag_fireap = 0;

/* 
 * dendrites
 * den_con_type, den_register: each bit carries each dendrite's value,
 * respectively, starting with LSB (with two MSBs left unused),
 * so corresponding PORTA pin names are: 0bXX543210
 * den_magnitudes: indexes correspond to PORTA pin names
 */
const uint8_t den_number = 6; // number of dendrites
const int16_t den_magnitudes[den_number] = { -20*COEFF_MV, 20*COEFF_MV, -20*COEFF_MV, 20*COEFF_MV, -20*COEFF_MV, 20*COEFF_MV }; // input magnitudes in mV
uint8_t den_register = 0; // read dendrite input register PINA into this var with "& DENDRITE_PINS_MASK"
uint8_t flag_refractoriness = 0;
int8_t stimulus_edge = 0; // 1 when dendritic stimulus added, -1 when removed, 0 if unchanged
int16_t den_v_offset = 0; // by how much the dendrites in total offset the membrane potential
int16_t den_v_valsum = 0; // membrane potential offset of the latest stimulus
int16_t den_v_countsum = 0;
int16_t den_stim_countadded = 0;
int16_t den_stim_countremoved = 0;
int16_t den_stim_valadded = 0;
int16_t den_stim_valremoved = 0;
volatile uint8_t den_register_volatile = 0; // ISR reads PINA into this var
volatile uint8_t flag_pinchange = 0; // 1 on pin change
unsigned long timer_ax = 0;
unsigned long ax_pulselen = 320;
uint8_t pin_state[den_number] = { 0 };
int8_t pin_edge[den_number] = { 0 };

unsigned long led_t_since_upd = millis();
unsigned long led_t_upd = 25;

/* read inputs, and save the membrane potential offset into den_v_offset */
int8_t den_refresh(void);

void axon_off(void);

/* outputs newV+den_v_offset, a new membrane potential value based on the
   current state of the dendrites */
int16_t get_v_target(int8_t stim_e);

/* hold a membrane potential for some time.
   basically a delay, with serial output updates */
void holdV(int16_t duration);

/* fire an action potential */
void fire_ap(int16_t duration=150);

/* set a membrane potential to a new value.
   "curve" refers to serial output, rgb led value
   is always changed linearly. */
void setV(int16_t newV, int16_t interval=10);

/* update the led based on the membrane potential */
void upd_led(int16_t potential=v_current);

/* turn led off or on, white, full brightness */
void led_off(void);
void led_white(void);

/* send the membrane potential value over USI */
void upd_ser(int16_t potential=v_current);

ISR (PCINT0_vect) {
    uint8_t tempregister = PINA;
    //~ delayMicroseconds(100);
    //~ if (PINA != tempregister) return; // debounce
    //~ else {
    flag_pinchange = 1;
    den_register_volatile = tempregister & DENDRITE_PINS_MASK;
    //~ }
}

void setup() {
    Palatis::SoftPWM.begin(120);
	
    cli();
    /* PA0-5 inputs, no pull-up (dendrites);
     * PA6 output, low (axon)
     * PA7 input, pull-up (unused)
     */
    DDRA  = 0b01000000;
    PORTA = 0b10000000;
    
    // enable pin change interrupts for dendrites (PA5 used for serial output)
    PCMSK0 = DENDRITE_PINS_MASK;
    GIMSK |= 1<<PCIE0;
    sei();
    
    // do a first read of the dendrites
    den_register_volatile = PINA & DENDRITE_PINS_MASK;
    den_refresh();
    
    upd_led();
    v_target = get_v_target(v_current);
    setV(v_target, 25);
}

void loop() {
    
    upd_led();
    axon_off();
    stimulus_edge = den_refresh();
    v_target = get_v_target(stimulus_edge);
    // if there's no new stimulus, or a stimulus stops
    if (stimulus_edge <= 0) { 
        /* hold the membrane potential for a bit. this prolongs
           EPSPs/IPSPs (new excitations/inhibitions) a bit */
        if ((!flag_refractoriness) && (stimulus_edge == -1)) {
            //~ if ((v_current >= v_target) == (den_v_valsum < 0)) {
            if ((v_current >= v_target) && (den_v_valsum < 0)) {
                holdV(600);
            } else if ((v_current < v_target) && (den_v_valsum > 0)) {
                holdV(590); // midpoint at around 600/550? (not 470) ratio
            }
        }
        flag_refractoriness = 0;
        /* if there was no pin change while holding, update v_current
           while tracing a sigmoid curve. otherwise, just go on */
        if (!flag_pinchange) {
            //v_target = get_v_target(stimulus_edge);
            //~ setV(v_target, 300);
            setV(v_target);
        }
    // else if a new stimulus is added
    } else if (stimulus_edge > 0) {
        //~ setV(v_target, 100);
        setV(v_target);
    }
    // flag_fireap gets set if V_THRESH gets reached. fire an AP if so.
    if (flag_fireap) fire_ap();
    delay(2);
}

int8_t den_refresh(void) {
    int16_t sum_total = 0, sum_countadded = 0, sum_countremoved = 0, sum_valadded = 0, sum_valremoved = 0;
    int8_t edge_sum = 0;
    uint8_t changed_pins;
    uint8_t new_register = den_register_volatile;
    
    flag_pinchange = 0;
    
    for (uint8_t i=0; i<den_number; i++) {
        // dendrites' states
        if ( new_register & (1<<i) ) {
            pin_state[i] = 1;
            sum_total += den_magnitudes[i];
        } else {
            pin_state[i] = 0;
        }
        
        // dendrites' edges
        if ( (new_register & (1<<i)) && !(den_register & (1<<i)) ) {
            pin_edge[i] = 1;
            edge_sum += 1;
            sum_countadded += 1;
            sum_valadded += den_magnitudes[i];
        } else if ( !(new_register & (1<<i)) && (den_register & (1<<i)) ) {
            pin_edge[i] = -1;
            edge_sum -= 1;
            sum_countremoved += 1;
            sum_valremoved += den_magnitudes[i];
        } else {
            pin_edge[i] = 0;
        }
    }
    den_stim_countadded = sum_countadded;
    den_stim_countremoved = sum_countremoved;
    den_stim_valadded = sum_valadded;
    den_stim_valremoved = sum_valremoved;
    den_v_offset = sum_total;
    den_v_countsum = sum_countadded-sum_countremoved;
    den_v_valsum = sum_valadded-sum_valremoved;
    den_register = new_register;
    return edge_sum;
}

void axon_off() {
    unsigned long current_time = millis();
    if ( (PORTA & (1<<PA6)) && ((current_time-timer_ax) > ax_pulselen) ) {
        PORTA &= ~(1<<PA6);
    }
}
    

int16_t get_v_target(int8_t stim_e) {
    /* 
     * returns a membrane potential value offset by
     * the current state of the dendritic inputs
     */
    int16_t v_input;
    int16_t v_input_offset;
    /* if a new stimulus has just been added, add its value to the
     * current potential. this allows for "temporal summation" of stimuli.
     * 
     * otherwise, just add the total offsets of all dendrites
     * to the resting potential value.
     */
    if (stim_e == 1) {
        v_input = v_current;
        v_input_offset = den_v_valsum;
    } else {
        v_input = v_values[INDEX_REST];
        v_input_offset = den_v_offset;
    }
    
    
    if (v_input_offset > 0) {
        v_input += v_input_offset;
        if (v_input > v_values[INDEX_THRESH]) {
            v_input = v_values[INDEX_THRESH];
        }
    } else if (v_input_offset < 0) {
        v_input += v_input_offset;
        if (v_input <= (-40*COEFF_MV)) {
            v_input = -40*COEFF_MV;
        }
    }
    return v_input;
}

void holdV(int16_t duration) {
    if (duration < 25) duration = 25;
    int16_t delay_count = duration/25;
    int16_t delay_mod = duration%25;
    for (uint8_t i=0; i<delay_count; ++i) {
        if (flag_pinchange) return;
        delay(25);
        upd_led();
        axon_off();
    }
    delay(delay_mod);
}

void fire_ap(int16_t duration=150) {
    if (duration < 5) duration = 5;
    flag_fireap = 0;
    led_off();
    axon_off();
    delay(130);
    led_white();
    delay(20);
    led_off();
    delay(duration-5);
    v_current = v_values[INDEX_MIN];
    upd_led();
    flag_refractoriness = 1;
    PORTA &= ~(1<<PA6);
    delay(5);
    PORTA |= (1<<PA6);
    timer_ax = millis();
}

void setV(int16_t newV, int16_t interval=10) {
    int16_t diff;
    //~ int16_t v_step;
    int8_t add = 1;
    //~ if (duration < 25) duration = 25;
    //~ int16_t steps = duration/25;
    
    diff = v_current-newV;
    if (!diff) return;
    if (diff > 0) add = -1;
    
    //~ v_step = diff/steps;
    //~ for (int16_t i=0; i<steps; i++) {
        //~ axon_off();
        //~ if (flag_pinchange) return;
        //~ v_current -= v_step;
        //~ if (v_current >= v_values[INDEX_THRESH]) {
            //~ v_current = v_values[INDEX_THRESH];
            //~ upd_led();
            //~ flag_fireap = 1;
            //~ return;
        //~ }
        //~ upd_led();
        //~ delay(25);
    //~ }
    
    while ((add < 0) == (v_current > newV)) {
        if (flag_pinchange) return;
        v_current += add*COEFF_MV;
        diff = v_current-newV;
        axon_off();
        if (v_current >= (v_values[INDEX_THRESH])) {
            v_current = v_values[INDEX_THRESH];
            upd_led();
            flag_fireap = 1;
            return;
        }
        upd_led();
        delay(interval);
    }        
    
    v_current = newV;
    upd_led();
}

void upd_led(int16_t potential=v_current) {
	/* 
	 * How LED levels change as V goes up from V_MIN to V_THRESH:
	 * 
	 *   ------------------------ V_THRESH
	 *  ^ R:50->100%  G:50->0%
	 *  ^------------------------ V_HALF_RT
	 *  ^ R:0->50%    G:100->50%
	 *  ^------------------------ V_REST
	 *  ^ G:50->100%  B:50->0%
	 *  ^------------------------ V_HALF_MR
	 *  ^ G:0->50%    B:100->50%
	 *  ^------------------------ V_MIN
	 * 
	 * look up exact values in v_values[] using
	 * INDEX_HALF_RT, INDEX_REST...
	 * 
	 */
    if ((millis() - led_t_since_upd) < led_t_upd) return;
    if (potential < 0) {
        potential = 0;
    }
	int16_t level;
    int16_t divider = 8;
    int16_t max_level = PWM_LEVELS/divider;
	if (potential > (v_values[INDEX_HALF_RT])) {
		level = (potential - v_values[INDEX_HALF_RT])/MULT_RES/divider;
		Palatis::SoftPWM.set(RED, (max_level/2)+level);
		Palatis::SoftPWM.set(GREEN, (max_level/2)-level);
		Palatis::SoftPWM.set(BLUE, 0);
	}
	else if (potential > (v_values[INDEX_REST])) {
		level = (potential - v_values[INDEX_REST])/MULT_RES/divider;
		Palatis::SoftPWM.set(RED, level);
		Palatis::SoftPWM.set(GREEN, max_level-level);
		Palatis::SoftPWM.set(BLUE, 0);
	}
	else if (potential > (v_values[INDEX_HALF_MR])) {
		level = (potential - v_values[INDEX_HALF_MR])/MULT_RES/divider;
		Palatis::SoftPWM.set(RED, 0);
		Palatis::SoftPWM.set(GREEN, (max_level/2)+level);
		Palatis::SoftPWM.set(BLUE, (max_level/2)-level);
	}
	else {
		level = potential/MULT_RES/divider;
		Palatis::SoftPWM.set(RED, 0);
		Palatis::SoftPWM.set(GREEN, level);
		Palatis::SoftPWM.set(BLUE, max_level-level);
	}
    led_t_since_upd = millis();
}

void led_off(void) {
    for (uint8_t i=0; i<3; ++i) Palatis::SoftPWM.set(i, 0);
}

void led_white(void) {
    for (uint8_t i=0; i<3; ++i) Palatis::SoftPWM.set(i, 255);
}

void upd_ser(int16_t potential=v_current) {
    return;
}
